## Usage

```
lein figwheel
```

Wait for figwheel to finish and notify browser of changed files! Then in another terminal tab:

```
lein run
```

point browser to:
http://localhost:8080

## Credits

Based on https://github.com/enterlab/rente

(ns hex.app
  (:require [clojure.tools.logging :as log]
            [hex.ws :as ws]))

(def terrain [:Plains :Mountains :Jungle])

(defn findHex
  [pos]
  {:pos pos :terrain (first terrain)})

(ns hex.client.views
  (:require
    [cljs.core.async :as async
             :refer [<! >! chan close! sliding-buffer put! alts! timeout]]
    [reagent.core :as reagent :refer [atom]]
    [hex.client.ws :as socket]
    [hex.client.util :as util]))

(def window-width (reagent/atom nil))

(defn draw-canvas-contents [canvas]
  (let [ ctx (.getContext canvas "2d")
        w (.-clientWidth canvas)
        h (.-clientHeight canvas)]
      (.beginPath ctx)
      (.moveTo ctx 0 0)
      (.lineTo ctx w h)
      (.moveTo ctx w 0)
      (.lineTo ctx 0 h)
      (.stroke ctx)
      (util/hex ctx {:q 0, :r 0})
      (util/hex ctx {:q 1, :r 1})
      (util/hex ctx {:q 1, :r 0})))

(defn on-window-resize [evt]
  (reset! window-width (.-innerWidth js/window)))

(.addEventListener js/window "resize" on-window-resize)

(defn mouse-handler [evt]
  (socket/tile {:x (.-x evt)}))

(defn main [data]
  (let [dom-node (reagent/atom nil)]
   (reagent/create-class
    {:component-did-update
     (fn [this]
       (.addEventListener @dom-node "mousedown" mouse-handler)
       (draw-canvas-contents (.-firstChild @dom-node)))

     :component-did-mount
     (fn [ this]
       (reset! dom-node (reagent/dom-node this)))

     :reagent-render
     (fn []
       @window-width ;; Trigger re-render on window resizes
       [:div.with-canvas
       ;; reagent-render is called before the compoment mounts, so
       ;; protect against the null dom-node that occurs on the first
       ;; render
        [:canvas (if-let [ node @dom-node]
                   {:width (.-innerWidth js/window)
                    :height (.-innerHeight js/window)})]])})))

(ns hex.client.util)
(enable-console-print!)

(def size 50)

(defn hex_corner [center i]
  (let [cx (:x center)
        cy (:y center)
        angle (* 60 i)
        rad (* Math/PI (/ angle 180))
        x (+ cx (* size (Math/cos rad)))
        y (+ cy (* size (Math/sin rad)))]
    ; (println {:x x, :y y} i angle rad center)
    {:x x :y y}))

(defn hexToPix [hex]
  (let [x (* size (/ 3 2) (:q hex))
        y (* size (Math/sqrt 3) (+ (:r hex) (/ (:q hex) 2)))]
    {:x x :y y}))

(defn hex-to-cube [hex]
  (let [x (:q hex)
          z (:r hex)
          y (- (* -1 x) z)]
    {:x x :y y :z z}))

(defn cube-to-hex [cube])

(defn cube-round [cube])

(defn hex-round [hex]
  (cube-to-hex (cube-round (hex-to-cube hex))))

(defn pixToHex [point]
  (let [x (:x point)
        y (:y point)
        q (* x (/ (/ 2 3) size))
        r (/ (-  (* (/ (Math/sqrt 3) 3) y)  (/ x 3)) size)]
    (hex-round {:q q :r r})))

(defn line [ctx start end]
  ; (println start end)
  (.moveTo ctx (:x start) (:y start))
  (.lineTo ctx (:x end) (:y end)))

(defn hex [ctx pos]
  (let [center (hexToPix pos)]
   (.save ctx)
   (println center)
   (.beginPath ctx)
   (doseq [i (range 0 6)]
     (line ctx
         (hex_corner center i)
         (hex_corner center (inc i))))
   (.stroke ctx)
   (.restore ctx)))
